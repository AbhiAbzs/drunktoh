using System;
using System.Collections.Generic;
using static System.Collections.Generic.Dictionary<TKey, TValue>;

namespace TOH_AfterParty
{
    class Program
    {
        static void Main(string[] args)
        {
            var ss = statMaths();

            Console.WriteLine("Step Count {0}", ss);
        }

        public void solve()
        {
            //benchmark();
            Console.WriteLine(statMaths());
        }

        //Working mathematical approach.
        static long statMaths()
        {
            long mod = (long)Math.Pow(10, 9);

            long total_expected_number = 0;
            long prev_g = 0, curr_g = 1;
            long k = 10, a = 3, b = 6, c = 9;
            for (int n = 1; n <= 10000; n++)
            {
                long expected_number = (2 * curr_g * (c - a) * (k - 1) - (2 * k - b - c) * (c - b)) % mod;
                total_expected_number = (total_expected_number + expected_number) % mod;
                var temp = prev_g;
                prev_g = curr_g;
                curr_g = (curr_g + 2 * temp + 1) % mod;
                k = (k * 10) % mod;
                a = (a * 3) % mod;
                b = (b * 6) % mod;
                c = (c * 9) % mod;
            }
            return total_expected_number;
        }

        public void benchmark()
        {

            for (int n = 1; n <= 10; n++)
            {
                Console.WriteLine($"n => {new TowerOfHanoi().simulate(n)}");
                Console.WriteLine(get_expected_number(2, 5, 1, 3, 5) == 60);
                Console.WriteLine(get_expected_number(3, 20, 4, 9, 17) == 2358);
            }
        }

        public int get_expected_number(int n, int k, int a, int b, int c)
        {
            var g = (int)(Math.Pow(2, (n + 2)) - 3 - Math.Pow((-1), n)) / 6;
            return 2 * g * (c - a) * (k - 1) - (2 * k - b - c) * (c - b);
        }
    }
}


class TowerOfHanoi
{
    public List<Tuple<int, int>> movement = new List<Tuple<int, int>>();

    public Dictionary<Tuple<int, int>, int> simulate(int n)
    {
        int a, b, c;
        a = 0; b = 1; c = 2;
        moveTower(n, a, c, b);
        return statistics(a, b, c);
    }

    public void moveTower(int height, int fromPeg, int toPeg, int withPeg)
    {
        if (height >= 1)
        {
            moveTower(height - 1, fromPeg, withPeg, toPeg);
            moveDisk(fromPeg, toPeg);
            moveTower(height - 1, withPeg, toPeg, fromPeg);
        }
    }

    public void moveDisk(int fromPeg, int toPeg)
    {
        movement.Add(new Tuple<int, int>(fromPeg, toPeg));
    }


    public Dictionary<Tuple<int, int>, int> statistics(int a, int b, int c)
    {
        Dictionary<Tuple<int, int>, int> stat = new Dictionary<Tuple<int, int>, int>();
        int movement_count = movement.Count;
        int currFromPeg, currToPeg, nextFromPeg, nextToPeg;
        for (int i = 0; i < movement_count - 1; i++)
        {
            currFromPeg = movement[i].Item1;
            currToPeg = movement[i].Item2;
            nextFromPeg = movement[i + 1].Item1;
            nextToPeg = movement[i + 1].Item2;
            foreach (var curr_movement in new List<Tuple<int, int>>() { new Tuple<int, int>(currFromPeg, currToPeg), new Tuple<int, int>(currToPeg, nextFromPeg) })
            {
                if (!stat.ContainsKey(curr_movement))
                    stat[curr_movement] = 0;
                stat[curr_movement] += 1;
            }
            var curr_movementLast = movement[movement.Count - 1];
            if (!stat.ContainsKey(curr_movementLast))
                stat[curr_movementLast] = 0;
            stat[curr_movementLast] += 1;
        }
        return mergeStatistics(stat, a, b, c);
    }
    public Dictionary<Tuple<int, int>, int> mergeStatistics(Dictionary<Tuple<int, int>, int> stat, int a, int b, int c)
    {

        if (stat.ContainsKey(new Tuple<int, int>(a, b)) && stat.ContainsKey(new Tuple<int, int>(b, c)) &&
            stat[new Tuple<int, int>(a, b)] == stat[new Tuple<int, int>(b, c)])
        {
            if (!stat.ContainsKey(new Tuple<int, int>(a, c)))
                stat[new Tuple<int, int>(a, c)] = 0;
            stat[new Tuple<int, int>(a, c)] += stat[new Tuple<int, int>(a, b)];
            if (stat.ContainsKey(new Tuple<int, int>(a, b)))
                stat.Remove(new Tuple<int, int>(a, b));
            if (stat.ContainsKey(new Tuple<int, int>(b, c)))
                stat.Remove(new Tuple<int, int>(b, c));
        }

        if (stat.ContainsKey(new Tuple<int, int>(c, b)) && stat.ContainsKey(new Tuple<int, int>(b, a)) &&
                   stat[new Tuple<int, int>(c, b)] == stat[new Tuple<int, int>(b, a)])
        {
            if (!stat.ContainsKey(new Tuple<int, int>(c, a)))
                stat[new Tuple<int, int>(c, a)] = 0;
            stat[new Tuple<int, int>(c, a)] += stat[new Tuple<int, int>(c, b)];
            if (stat.ContainsKey(new Tuple<int, int>(c, b)))
                stat.Remove(new Tuple<int, int>(c, b));
            if (stat.ContainsKey(new Tuple<int, int>(b, a)))
                stat.Remove(new Tuple<int, int>(b, a));
        }
        return stat;
    }
}