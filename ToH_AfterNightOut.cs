﻿using System;

namespace ToH_AfterNightOut
{
    class ToH_AfterNightOut
    {
        static void Main(string[] args)
        {
            long n = 10000;
            long k = 3, a = 0, b = 1, c = 2;
            //long k = 10, a = 3, b = 6, c = 9;
            Console.WriteLine($"\n{"----------------------------"}Tower of Hanoi - After Nightout{"----------------------------"}\n\n");
            Console.WriteLine($"Number of steps count for ∑1≤n≤10000 E(n,10n,3n,6n,9n):\t{e(n, k, a, b, c)}");
            Console.WriteLine($"Number of steps count for ∑1≤n≤10000 E(n,10^n,3^n,6^n,9^n):\t{statMathsForPower()}");

            ///The following approach uses reccursion for computing the values, but fails to achieve the same task, as we all know that the Famous ToH problem is among that set of problem which can only be computed with infinite time & infine resource(Memory).
            //new ToH_AfterNightOut().solve();    //computationally expensive, can't be achieved with limited time & resource(Memory).

            ///Dynamic Programming approach -> Using this approach this class of problem can be solved in limited time, with infinite resource(Memory).
            //dynamicSol4ToH()    //computationally expensive, can't be achieved with limited resource(Memory).[still need to implement]
        }
        public void solve(int diskCount)
        {
            benchmark(diskCount);
        }

        public void benchmark(int dickCount)
        {
            for (int n = 1; n <= dickCount; n++)
            {
                var StepStats = new TowerOfHanoi_Algo().simulate(n);
                PrintTOH(n, StepStats);
            }
            //Console.WriteLine(get_expected_number(2, 5, 1, 3, 5) == 60);
            //Console.WriteLine(get_expected_number(3, 20, 4, 9, 17) == 2358);
        }

        public void PrintTOH(int n, Dictionary<Tuple<int, int>, int> StepStats)
        {
            Console.Write($"{n} => ");
            foreach (var item in StepStats)
            {
                Console.Write("{");
                Console.Write($"{ item.Key.Item1}, {item.Key.Item2}) : {item.Value}");
                Console.Write("}");
            }
            Console.WriteLine();
        }

        public long get_expected_number(long n, long k, long a, long b, long c)
        {
            long res;

            //var g = (long)(Math.Pow(2, (n + 2)) - 3 - Math.Pow((-1), n)) / 6;
            //res  =(2 * g * (c - a) * (k - 1) - (2 * k - b - c) * (c - b)) ;
            //var result  =(2 * g * (c - a) * (k - 1) - (2 * k - b - c) * (c - b));

            var g = (PowerKePapa(2, (int)(n + 2)) - 3 - PowerKePapa((-1), (int)n)) / 6;
            res = (2 * g * (c - a) * (k - 1) - (2 * k - b - c) * (c - b)) % 1000000000;
            var result = (2 * g * (c - a) * (k - 1) - (2 * k - b - c) * (c - b)) % 1000000000;
            return res;
        }


        /// <summary>
        /// refrence http://mathworld.wolfram.com/TowerofHanoi.html
        /// Working mathemetical aaroch.
        /// S(n)={S(n-1),n,S(n-1)} 
        /// </summary>
        static long e(long noOfDisk, long kSquareTiles, long source, long auxiliary, long destination)
        {
            long mod = (long)Math.Pow(10, 9);
            long finalStepCount = 0;
            long lastSofN = 0, currentSofN = 1;

            for (int i = 1; i <= noOfDisk; i++)
            {
                long bobStepCount = (2 * currentSofN * (destination - source) * (kSquareTiles - 1) - (2 * kSquareTiles - auxiliary - destination) * (destination - auxiliary)) % mod;
                finalStepCount = (finalStepCount + bobStepCount) % mod;
                var temp = lastSofN;
                lastSofN = currentSofN;
                currentSofN = (currentSofN + 2 * temp + 1) % mod;

                kSquareTiles = (10 * i) % mod;
                source = (3 * i) % mod;
                auxiliary = (6 * i) % mod;
                destination = (9 * i) % mod;
            }
            return finalStepCount;
        }

        /// <summary>
        /// refrence http://mathworld.wolfram.com/TowerofHanoi.html
        /// Working mathemetical approach.
        /// S(n)={S(n-1),n,S(n-1)} 
        /// </summary>
        static long statsMathsForPower()
        {
            long mod = (long)Math.Pow(10, 9);

            long finalStepCount = 0;
            long lastSofN = 0, CurrentSofN = 1;
            long k = 10, a = 3, b = 6, c = 9;
            for (int n = 1; n <= 10000; n++)
            {
                long bobStepCount = (2 * CurrentSofN * (c - a) * (k - 1) - (2 * k - b - c) * (c - b)) % mod;
                finalStepCount = (finalStepCount + bobStepCount) % mod;
                var temp = lastSofN;
                lastSofN = CurrentSofN;
                CurrentSofN = (CurrentSofN + 2 * temp + 1) % mod;

                k = (k * 10) % mod;
                a = (a * 3) % mod;
                b = (b * 6) % mod;
                c = (c * 9) % mod;
            }
            return finalStepCount;
        }
    }

    class TowerOfHanoi_Algo
    {
        public List<Tuple<int, int>> movement = new List<Tuple<int, int>>();

        public Dictionary<Tuple<int, int>, int> simulate(int n)
        {
            int a, b, c;
            a = 1; b = 3; c = 5;
            movement.Add(new Tuple<int, int>(b, a));    //For handling the 1st move of 'B' to 'A'
            moveTower(n, a, c, b);
            return statistics(a, b, c);
        }

        public void moveTower(int height, int fromPeg, int toPeg, int withPeg)
        {
            if (height >= 1)
            {
                moveTower(height - 1, fromPeg, withPeg, toPeg);
                moveDisk(fromPeg, toPeg);
                moveTower(height - 1, withPeg, toPeg, fromPeg);
            }
        }


        public void moveDisk(int fromPeg, int toPeg)
        {
            movement.Add(new Tuple<int, int>(fromPeg, toPeg));
        }


        public Dictionary<Tuple<int, int>, int> statistics(int a, int b, int c)
        {
            Dictionary<Tuple<int, int>, int> stat = new Dictionary<Tuple<int, int>, int>();
            int movement_count = movement.Count;
            int currFromPeg, currToPeg, nextFromPeg, nextToPeg;
            for (int i = 0; i < movement_count - 1; i++)
            {
                currFromPeg = movement[i].Item1;
                currToPeg = movement[i].Item2;
                nextFromPeg = movement[i + 1].Item1;
                nextToPeg = movement[i + 1].Item2;
                foreach (var curr_movement in new List<Tuple<int, int>>() { new Tuple<int, int>(currFromPeg, currToPeg), new Tuple<int, int>(currToPeg, nextFromPeg) })
                {
                    if (!stat.ContainsKey(curr_movement))
                        stat[curr_movement] = 0;
                    stat[curr_movement] += Math.Abs(curr_movement.Item1 - curr_movement.Item2);
                }
            }
            var curr_movementLast = movement[movement.Count - 1];
            if (!stat.ContainsKey(curr_movementLast))
                stat[curr_movementLast] = 0;
            stat[curr_movementLast] += Math.Abs(curr_movementLast.Item1 - curr_movementLast.Item2);
            // GetMoves(stat);  //not worked
            return mergeStatistics(stat, a, b, c);  //Not required any more
        }
        public Dictionary<Tuple<int, int>, int> mergeStatistics(Dictionary<Tuple<int, int>, int> stat, int a, int b, int c)
        {
            //Not required nay more.
            if (stat.ContainsKey(new Tuple<int, int>(a, b)) && stat.ContainsKey(new Tuple<int, int>(b, c)) &&
                stat[new Tuple<int, int>(a, b)] == stat[new Tuple<int, int>(b, c)])
            {
                if (!stat.ContainsKey(new Tuple<int, int>(a, c)))
                    stat[new Tuple<int, int>(a, c)] = 0;
                stat[new Tuple<int, int>(a, c)] += stat[new Tuple<int, int>(a, b)];
                if (stat.ContainsKey(new Tuple<int, int>(a, b)))
                    stat.Remove(new Tuple<int, int>(a, b));
                if (stat.ContainsKey(new Tuple<int, int>(b, c)))
                    stat.Remove(new Tuple<int, int>(b, c));
            }

            if (stat.ContainsKey(new Tuple<int, int>(c, b)) && stat.ContainsKey(new Tuple<int, int>(b, a)) &&
                       stat[new Tuple<int, int>(c, b)] == stat[new Tuple<int, int>(b, a)])
            {
                if (!stat.ContainsKey(new Tuple<int, int>(c, a)))
                    stat[new Tuple<int, int>(c, a)] = 0;
                stat[new Tuple<int, int>(c, a)] += stat[new Tuple<int, int>(c, b)];
                if (stat.ContainsKey(new Tuple<int, int>(c, b)))
                    stat.Remove(new Tuple<int, int>(c, b));
                if (stat.ContainsKey(new Tuple<int, int>(b, a)))
                    stat.Remove(new Tuple<int, int>(b, a));
            }
            return stat;
        }

        //test approach(not worked)
        /*public void GetMoves(Dictionary<Tuple<int, int>, int> stat)
        {
            int k = 5;
            double pathCost = 0;
            foreach (var item in stat)
            {
                pathCost += Cost(k, item.Key.Item1, item.Key.Item2);
            }

        }
        public double Cost(int k, int x, int y)
        {
            double cst;
            if (x < y)
            {
                cst = (Math.Pow(2, (y - 1))) - (Math.Pow(2, (x - 1)));
            }
            else
            {
                cst = (Math.Pow(2, (k - y))) - (Math.Pow(2, (k - x)));
            }
            return cst;
        }*/
    }
}
