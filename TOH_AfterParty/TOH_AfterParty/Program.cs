﻿using System;
using System.Collections.Generic;
//using static System.Collections.Generic.Dictionary<TKey, TValue>;

namespace TOH_AfterParty
{
    class Program
    {
        //static void Main(string[] args)
        //{

        //    //Console.WriteLine("Step Requried: ", statMathsForPower());
        //    Console.WriteLine("Step Requried: {0}", statMathsForMultiplication());
        //    //new Program().solve();
        //    long total = 0;
        //    long k1, a1, b1, c1, ss;
        //    for (int i = 1; i <= 10000; i++)
        //    {
        //        k1 = PowerKePapa(10, i);
        //        a1 = PowerKePapa(3, i);
        //        b1 = PowerKePapa(6, i);
        //        c1 = PowerKePapa(9, i);
        //        ss = new Program().get_expected_number(i, k1, a1, b1, c1);
        //        total += ss;
        //    }

        //}

        static long PowerKePapa(int value, int power)
        {
            long result = value;
            for (int i = 1; i < power; i++)
            {
                result = (result * value) % 1000000000;
            }
            return result;
        }

        public void solve()
        {
            benchmark();
            Console.WriteLine(statMathsForPower());
        }

        //Working mathemetical aaroch.
        /// <summary>
        /// refrence http://mathworld.wolfram.com/TowerofHanoi.html
        /// S(n)={S(n-1),n,S(n-1)} 
        /// </summary>

        //  var g = (int)(Math.Pow(2, (n + 2)) - 3 - Math.Pow((-1), n)) / 6;
        //    return 2 * g* (c - a) * (k - 1) - (2 * k - b - c) * (c - b);
        static long statMathsForPower()
        {
            long mod = (long)Math.Pow(10, 9);

            long finalStepCount = 0;
            long lastSofN = 0, CurrentSofN = 1;
            long k = 10, a = 3, b = 6, c = 9;
            for (int n = 1; n <= 10000; n++)
            {
                long bobStepCount = (2 * CurrentSofN * (c - a) * (k - 1) - (2 * k - b - c) * (c - b)) % mod;
                var k1 = ((long)(Math.Pow(10, n)) % mod);
                var a1 = (long)(Math.Pow(3, n) % mod);
                var b1 = (long)(Math.Pow(6, n) % mod);
                var c1 = (long)(Math.Pow(9, n) % mod);
                //long bobStepCount = (2 * CurrentSofN * (c1 - a1) * (k1 - 1) - (2 * k1 - b1 - c1) * (c1 - b1)) % mod;
                finalStepCount = (finalStepCount + bobStepCount) % mod;
                var temp = lastSofN;
                lastSofN = CurrentSofN;
                CurrentSofN = (CurrentSofN + 2 * temp + 1) % mod;

                k = (k * 10) % mod;
                a = (a * 3) % mod;
                b = (b * 6) % mod;
                c = (c * 9) % mod;
            }
            return finalStepCount;
        }

        static long statMathsForMultiplication()
        {
            long mod = (long)Math.Pow(10, 9);

            long finalStepCount = 0;
            long lastSofN = 0, CurrentSofN = 1;
            long k = 10, a = 3, b = 6, c = 9;
            for (int n = 1; n <= 10000; n++)
            {
                long bobStepCount = (2 * CurrentSofN * (c - a) * (k - 1) - (2 * k - b - c) * (c - b)) % mod;
                finalStepCount = (finalStepCount + bobStepCount) % mod;
                var temp = lastSofN;
                lastSofN = CurrentSofN;
                CurrentSofN = (CurrentSofN + 2 * temp + 1) % mod;

                k = (10 * n) % mod;
                a = (3 * n) % mod;
                b = (6 * n) % mod;
                c = (9 * n) % mod;
            }
            return finalStepCount;
        }

        public void benchmark()
        {
            for (int n = 9992; n <= 10000; n++)
            {
                var StepStats = new TowerOfHanoi().simulate(n);
                PrintTOH(n, StepStats);
                //Console.WriteLine(get_expected_number(2, 5, 1, 3, 5) == 60);
                //Console.WriteLine(get_expected_number(3, 20, 4, 9, 17) == 2358);
            }

        }

        public void PrintTOH(int n, Dictionary<Tuple<int, int>, int> StepStats)
        {
            Console.Write($"{n} => ");
            foreach (var item in StepStats)
            {
                Console.Write("{");
                Console.Write($"{ item.Key.Item1}, {item.Key.Item2}) : {item.Value}");
                Console.Write("}");
            }
            Console.WriteLine();
        }


        public long get_expected_number(long n, long k, long a, long b, long c)
        {
            long res;

            //var g = (long)(Math.Pow(2, (n + 2)) - 3 - Math.Pow((-1), n)) / 6;
            //res  =(2 * g * (c - a) * (k - 1) - (2 * k - b - c) * (c - b)) ;
            //var result  =(2 * g * (c - a) * (k - 1) - (2 * k - b - c) * (c - b));

            var g = (PowerKePapa(2, (int)(n + 2)) - 3 - PowerKePapa((-1), (int)n)) / 6;
            res = (2 * g * (c - a) * (k - 1) - (2 * k - b - c) * (c - b)) % 1000000000;
            var result = (2 * g * (c - a) * (k - 1) - (2 * k - b - c) * (c - b)) % 1000000000;
            return res;
        }
    }
}



class TowerOfHanoi
{
    public List<Tuple<int, int>> movement = new List<Tuple<int, int>>();

    public Dictionary<Tuple<int, int>, int> simulate(int n)
    {
        int a, b, c;
        a = 0; b = 1; c = 2;
        moveTower(n, a, c, b);
        return statistics(a, b, c);
    }

    public void moveTower(int height, int fromPeg, int toPeg, int withPeg)
    {
        if (height >= 1)
        {
            moveTower(height - 1, fromPeg, withPeg, toPeg);
            moveDisk(fromPeg, toPeg);
            moveTower(height - 1, withPeg, toPeg, fromPeg);
        }
    }

    public void moveDisk(int fromPeg, int toPeg)
    {
        movement.Add(new Tuple<int, int>(fromPeg, toPeg));
    }


    public Dictionary<Tuple<int, int>, int> statistics(int a, int b, int c)
    {
        Dictionary<Tuple<int, int>, int> stat = new Dictionary<Tuple<int, int>, int>();
        int movement_count = movement.Count;
        int currFromPeg, currToPeg, nextFromPeg, nextToPeg;
        for (int i = 0; i < movement_count - 1; i++)
        {
            currFromPeg = movement[i].Item1;
            currToPeg = movement[i].Item2;
            nextFromPeg = movement[i + 1].Item1;
            nextToPeg = movement[i + 1].Item2;
            foreach (var curr_movement in new List<Tuple<int, int>>() { new Tuple<int, int>(currFromPeg, currToPeg), new Tuple<int, int>(currToPeg, nextFromPeg) })
            {
                if (!stat.ContainsKey(curr_movement))
                    stat[curr_movement] = 0;
                stat[curr_movement] += 1;
            }
        }
        var curr_movementLast = movement[movement.Count - 1];
        if (!stat.ContainsKey(curr_movementLast))
            stat[curr_movementLast] = 0;
        stat[curr_movementLast] += 1;
        return mergeStatistics(stat, a, b, c);
    }
    public Dictionary<Tuple<int, int>, int> mergeStatistics(Dictionary<Tuple<int, int>, int> stat, int a, int b, int c)
    {

        if (stat.ContainsKey(new Tuple<int, int>(a, b)) && stat.ContainsKey(new Tuple<int, int>(b, c)) &&
            stat[new Tuple<int, int>(a, b)] == stat[new Tuple<int, int>(b, c)])
        {
            if (!stat.ContainsKey(new Tuple<int, int>(a, c)))
                stat[new Tuple<int, int>(a, c)] = 0;
            stat[new Tuple<int, int>(a, c)] += stat[new Tuple<int, int>(a, b)];
            if (stat.ContainsKey(new Tuple<int, int>(a, b)))
                stat.Remove(new Tuple<int, int>(a, b));
            if (stat.ContainsKey(new Tuple<int, int>(b, c)))
                stat.Remove(new Tuple<int, int>(b, c));
        }

        if (stat.ContainsKey(new Tuple<int, int>(c, b)) && stat.ContainsKey(new Tuple<int, int>(b, a)) &&
                   stat[new Tuple<int, int>(c, b)] == stat[new Tuple<int, int>(b, a)])
        {
            if (!stat.ContainsKey(new Tuple<int, int>(c, a)))
                stat[new Tuple<int, int>(c, a)] = 0;
            stat[new Tuple<int, int>(c, a)] += stat[new Tuple<int, int>(c, b)];
            if (stat.ContainsKey(new Tuple<int, int>(c, b)))
                stat.Remove(new Tuple<int, int>(c, b));
            if (stat.ContainsKey(new Tuple<int, int>(b, a)))
                stat.Remove(new Tuple<int, int>(b, a));
        }
        return stat;
    }
}