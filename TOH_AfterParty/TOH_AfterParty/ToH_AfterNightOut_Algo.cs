﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TOH_AfterParty
{
    class ToH_AfterNightOut_Algo
    {
        static void Main(string[] args)
        {
            new ToH_AfterNightOut_Algo().solve();
        }

        public void solve()
        {
            benchmark();
        }

        public void benchmark()
        {
            for (int n = 1; n <= 10; n++)
            {
                var StepStats = new TowerOfHanoi_Algo().simulate(n);
                PrintTOH(n, StepStats);
            }
        }

        public void PrintTOH(int n, Dictionary<Tuple<int, int>, int> StepStats)
        {
            Console.Write($"{n} => ");
            foreach (var item in StepStats)
            {
                Console.Write("{");
                Console.Write($"{ item.Key.Item1}, {item.Key.Item2}) : {item.Value}");
                Console.Write("}");
            }
            Console.WriteLine();
        }
    }
}


class TowerOfHanoi_Algo
{
    public List<Tuple<int, int>> movement = new List<Tuple<int, int>>();

    public Dictionary<Tuple<int, int>, int> simulate(int n)
    {
        int a, b, c;
        a = 1; b = 3; c =5 ;
        //movement.Add(new Tuple<int, int>(b, a));
        moveTower(n, a, c, b);
        return statistics(a, b, c);
    }

    public void moveTower(int height, int fromPeg, int toPeg, int withPeg)
    {
        if (height >= 1)
        {
            moveTower(height - 1, fromPeg, withPeg, toPeg);
            moveDisk(fromPeg, toPeg);
            moveTower(height - 1, withPeg, toPeg, fromPeg);
        }
    }

    
    public void moveDisk(int fromPeg, int toPeg)
    {
        movement.Add(new Tuple<int, int>(fromPeg, toPeg));
    }


    public Dictionary<Tuple<int, int>, int> statistics(int a, int b, int c)
    {
        Dictionary<Tuple<int, int>, int> stat = new Dictionary<Tuple<int, int>, int>();
        int movement_count = movement.Count;
        int currFromPeg, currToPeg, nextFromPeg, nextToPeg;
        for (int i = 0; i < movement_count - 1; i++)
        {
            currFromPeg = movement[i].Item1;
            currToPeg = movement[i].Item2;
            nextFromPeg = movement[i + 1].Item1;
            nextToPeg = movement[i + 1].Item2;
            foreach (var curr_movement in new List<Tuple<int, int>>() { new Tuple<int, int>(currFromPeg, currToPeg), new Tuple<int, int>(currToPeg, nextFromPeg) })
            {
                if (!stat.ContainsKey(curr_movement))
                    stat[curr_movement] = 0;
                stat[curr_movement] += Math.Abs(curr_movement.Item1 - curr_movement.Item2);
            }
        }
        var curr_movementLast = movement[movement.Count - 1];
        if (!stat.ContainsKey(curr_movementLast))
            stat[curr_movementLast] =0;
        stat[curr_movementLast] += Math.Abs(curr_movementLast.Item1 - curr_movementLast.Item2);
        GetMoves(stat);
        return mergeStatistics(stat, a, b, c);
    }
    public Dictionary<Tuple<int, int>, int> mergeStatistics(Dictionary<Tuple<int, int>, int> stat, int a, int b, int c)
    {

        if (stat.ContainsKey(new Tuple<int, int>(a, b)) && stat.ContainsKey(new Tuple<int, int>(b, c)) &&
            stat[new Tuple<int, int>(a, b)] == stat[new Tuple<int, int>(b, c)])
        {
            if (!stat.ContainsKey(new Tuple<int, int>(a, c)))
                stat[new Tuple<int, int>(a, c)] = 0;
            stat[new Tuple<int, int>(a, c)] += stat[new Tuple<int, int>(a, b)];
            if (stat.ContainsKey(new Tuple<int, int>(a, b)))
                stat.Remove(new Tuple<int, int>(a, b));
            if (stat.ContainsKey(new Tuple<int, int>(b, c)))
                stat.Remove(new Tuple<int, int>(b, c));
        }

        if (stat.ContainsKey(new Tuple<int, int>(c, b)) && stat.ContainsKey(new Tuple<int, int>(b, a)) &&
                   stat[new Tuple<int, int>(c, b)] == stat[new Tuple<int, int>(b, a)])
        {
            if (!stat.ContainsKey(new Tuple<int, int>(c, a)))
                stat[new Tuple<int, int>(c, a)] = 0;
            stat[new Tuple<int, int>(c, a)] += stat[new Tuple<int, int>(c, b)];
            if (stat.ContainsKey(new Tuple<int, int>(c, b)))
                stat.Remove(new Tuple<int, int>(c, b));
            if (stat.ContainsKey(new Tuple<int, int>(b, a)))
                stat.Remove(new Tuple<int, int>(b, a));
        }
        return stat;
    }
    public void GetMoves(Dictionary<Tuple<int, int>, int> stat)
    {
        int k = 5;
        double pathCost = 0;
        foreach (var item in stat)
        {
            pathCost += Cost(k, item.Key.Item1, item.Key.Item2);
        }

    }
    public double Cost(int k, int x, int y)
    {
        double cst;
        if (x < y)
        {
            cst = (Math.Pow(2, (y - 1))) - (Math.Pow(2, (x - 1)));
        }
        else
        {
            cst = (Math.Pow(2, (k - y))) - (Math.Pow(2, (k - x)));
        }
        return cst;
    }
}
