﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TOH_AfterParty
{
    class ToH_AfterNightOut
    {
        //static void Main(string[] args)
        //{
        //    long n = 10000;
        //    //long k = 10, a = 3, b = 6, c = 9;
        //    long k = 3, a = 0, b = 1, c = 2;
        //    //Console.WriteLine(e(n, k, a, b, c));
        //    Console.WriteLine(statMathsForPower());
        //}


        /// <summary>
        /// refrence http://mathworld.wolfram.com/TowerofHanoi.html
        /// Working mathemetical aaroch.
        /// S(n)={S(n-1),n,S(n-1)} 
        /// </summary>
        static long e(long noOfDisk, long kSquareTiles, long source, long auxiliary, long destination)
        {
            long mod = (long)Math.Pow(10, 9);
            long finalStepCount = 0;
            long lastSofN = 0, currentSofN = 1;

            for (int i = 1; i <= noOfDisk; i++)
            {
                long bobStepCount = (2 * currentSofN * (destination - source) * (kSquareTiles - 1) - (2 * kSquareTiles - auxiliary - destination) * (destination - auxiliary)) % mod;
                finalStepCount = (finalStepCount + bobStepCount) % mod;
                var temp = lastSofN;
                lastSofN = currentSofN;
                currentSofN = (currentSofN + 2 * temp + 1) % mod;

                kSquareTiles = (10 * i) % mod;
                source = (3 * i) % mod;
                auxiliary = (6 * i) % mod;
                destination = (9 * i) % mod;
            }
            return finalStepCount;
        }

        /// <summary>
        /// refrence http://mathworld.wolfram.com/TowerofHanoi.html
        /// Working mathemetical aaroch.
        /// S(n)={S(n-1),n,S(n-1)} 
        /// </summary>
        static long statMathsForPower()
        {
            long mod = (long)Math.Pow(10, 9);

            long finalStepCount = 0;
            long lastSofN = 0, CurrentSofN = 1;
            long k = 10, a = 3, b = 6, c = 9;
            for (int n = 1; n <= 10000; n++)
            {
                long bobStepCount = (2 * CurrentSofN * (c - a) * (k - 1) - (2 * k - b - c) * (c - b)) % mod;
                var k1 = ((long)(Math.Pow(10, n)) % mod);
                var a1 = (long)(Math.Pow(3, n) % mod);
                var b1 = (long)(Math.Pow(6, n) % mod);
                var c1 = (long)(Math.Pow(9, n) % mod);
                //long bobStepCount = (2 * CurrentSofN * (c1 - a1) * (k1 - 1) - (2 * k1 - b1 - c1) * (c1 - b1)) % mod;
                finalStepCount = (finalStepCount + bobStepCount) % mod;
                var temp = lastSofN;
                lastSofN = CurrentSofN;
                CurrentSofN = (CurrentSofN + 2 * temp + 1) % mod;

                k = (k * 10) % mod;
                a = (a * 3) % mod;
                b = (b * 6) % mod;
                c = (c * 9) % mod;
            }
            return finalStepCount;
        }
    }

}
